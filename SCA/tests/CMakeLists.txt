link_directories(
  ${CMAKE_BUILD_DIR}/build/loop_functions/evolution_loop_functions
  ${CMAKE_BINARY_DIR}/controllers/footbot_nn
)
add_executable(sca_compare test.cpp)

target_link_libraries(sca_compare
  ${GALIB_LIBRARIES}
  footbot_nn
  evolution_loop_functions
  argos3core_simulator)

