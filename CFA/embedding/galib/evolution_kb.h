//Header constant file for the evolution experiment
// Constants.h

#ifndef EVO_CONSTANTS_H
#define EVO_CONSTANTS_H

const unsigned int NUM_FOOTBOT = 20;
const unsigned int NUM_TRIAL = 3;
const unsigned int CIRCLE_NUMBER = 2;
const float CIRCLE_RADIUS = 0.69f;
const float RECT_WIDTH = 1.38f;
const float RECT_HEIGTH = 0.35f;
const float MIN_RANGE_POS = -2.0f;
const float MAX_RANGE_POS = 2.0f;

const unsigned int NN_INPUTS=52;
const unsigned int NN_OUTPUTS=2;

const unsigned int POPULATION_SIZE = 5;
const unsigned int N_GENERATIONS = 5;
const unsigned int N_BEST_GENOMES = 0;		//WE MUST SET TO ZERO!
const float PROB_MUTATION = 0.05f;
const float PROB_CROSSOVER = 0.0f;
const float FOOTBOT_RADIUS= 0.1f;
const float ARENA_RADIUS = 2.75f;
const unsigned int ELITISM_FRACTION = 5; 	//it represents the fraction of the population that will be 
											//maintained in the next population (in this case -> 1/5)

const std::string RES_DIR("results/");
const std::string GENOMES_DIR("genomes/");
const std::string DATA_DIR("data/");
const std::string CONFIGS_DIR("configs/");
#endif
