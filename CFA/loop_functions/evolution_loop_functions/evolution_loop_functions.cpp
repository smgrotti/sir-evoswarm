#include "evolution_loop_functions.h"

/****************************************/
/****************************************/

bool isVisual = false;

CEvolutionLoopFunctions::CEvolutionLoopFunctions() :
	m_unCurrentTrial(0),
	//collection di posizioni una per ogni footbot per ogni trial
	m_pfControllerParams(new Real[GENOME_SIZE]),
	m_pcRNG(NULL),
	performance(0),
	m_pcFloor(NULL){}

/****************************************/
/****************************************/

CEvolutionLoopFunctions::~CEvolutionLoopFunctions() {
   delete[] m_pfControllerParams;
   //dobbiamo delete dei footbot e controller creati?? o lo fa il sim?
}

/****************************************/
/****************************************/

void CEvolutionLoopFunctions::Init(TConfigurationNode& t_node) {
	/*
	 * Create the random number generator, it is managed with the string "argos"
	 */
	m_pcRNG = CRandom::CreateRNG("argos");
	/*
	 *Initialize the data for the floor of the arena -> 2 circles and a rectangle
	 */
	/* Get a pointer to the floor entity */
	m_pcFloor = &GetSpace().GetFloorEntity();
	
	m_cCirclePos.push_back(CVector2(0.0f,-1.41f));
	m_cCirclePos.push_back(CVector2(1.0f,1.0f));
	m_cCirclePos.push_back(CVector2(-1.0f,1.0f));



	/*
	 * Create the foot-bot and get a reference to its controller.
	 */
	for(int i=0;i<NUM_FOOTBOT;i++){
		std::ostringstream stream;
		stream << "fb_" << i;
		std::string id = stream.str(); //new footbot id
		m_pcFootBot.push_back( new CFootBotEntity(
			id, // entity id
			"fnn" // controller id as set in the XML
			)
		);
		AddEntity(*(m_pcFootBot[i]));
		m_pcController.push_back(&dynamic_cast<CFootBotNNController&>(m_pcFootBot[i]->GetControllableEntity().GetController()));
	}
	
	/*
	for(int j=0; j<NUM_FOOTBOT; j++){
		LOG << "*****INIT****"<< j <<"foot bot POS dopo add <"
		<< m_pcFootBot[j]->GetEmbodiedEntity().GetOriginAnchor().Position.GetX()
		<< ">, <"
		<< m_pcFootBot[j]->GetEmbodiedEntity().GetOriginAnchor().Position.GetY()
		<< ">"
		<< std::endl;
	}
	*/
	
	//generate the positions randomly for each trial
	GeneratePositions(50);
	
	argos::LOG << "Size of position vector"<< m_vecInitSetup.size() << std::endl;
	try {
		GetNodeAttribute(t_node, "trial", m_unCurrentTrial);
		isVisual = true;
		Reset();
	}
	catch(CARGoSException& ex) {}

	//TRY
	int nbox = GetSpace().GetEntitiesByType("box").size();
	int nfootbot = GetSpace().GetEntitiesByType("foot-bot").size();
	LOG << "N Boxes = " << nbox << " and n footbots = " << nfootbot << std::endl;
}

void CEvolutionLoopFunctions::GeneratePositions(UInt32 max_trial) {
	//NUOVA CON COLLISION
	CRadians cOrient;
	argos::LOG << "GenPos start."<< std::endl;
	//range for positions
	CRange<Real> range;
	range.SetMin(MIN_RANGE_POS);
	range.SetMax(MAX_RANGE_POS);
	CRange<Real> rangeRadius;
	rangeRadius.SetMin(0.0f);
	rangeRadius.SetMax(ARENA_RADIUS - FOOTBOT_RADIUS);
	CRange<Real> angles;
	angles.SetMin(0.0f);
	angles.SetMax(2.0f);
	for(int i=0; i<NUM_TRIAL; i++){//for each trial
		argos::LOG << "GenPos foreach trial " << i <<"start."<< std::endl;
		for(int j=0; j<NUM_FOOTBOT; j++){//for each footbot
			//argos::LOG << "GenPos foreach fb " << j <<" start."<< std::endl;
			SInitSetup fbSetup; // setup info for footbot
			//generate setup random orientation
			cOrient = m_pcRNG->Uniform(CRadians::UNSIGNED_RANGE);
			//set the setup orientation
			fbSetup.Orientation.FromEulerAngles(
				cOrient,        // rotation around Z
				CRadians::ZERO, // rotation around Y
				CRadians::ZERO  // rotation around X
			);
			Real posX;
			Real posY;
			Real radius;
			Real angle;
			bool collide= false;
			int trials = 0;
			do{
				//argos::LOG << "GenPos foreach try "<< trials << " footbot " << j <<std::endl;
				/*posX = m_pcRNG->Uniform(range);
				posY = m_pcRNG->Uniform(range);
				argos::LOG <<"xPos is "<<posX<< " yPos is "<<posY<< std::endl;
				fbSetup.Position.SetX(posX);
				fbSetup.Position.SetY(posY);
				fbSetup.Position.SetZ(0);*/
				radius = m_pcRNG->Uniform(rangeRadius); // radius
				angle = m_pcRNG->Uniform(angles); // angle
				fbSetup.Position.FromSphericalCoords(
					radius,                                          // distance from origin
					CRadians::PI_OVER_TWO,                         // angle with Z axis
					angle * CRadians::PI // rotation around Z
					);
				fbSetup.Position.SetZ(0);
				trials++;// increase the trial counter
				collide = Collision(fbSetup, j , i);
			} while (collide && trials < max_trial);
			if(trials >= max_trial){
				argos::LOG <<"numero guess superato nel trial "<< m_unCurrentTrial <<" per il footbot "<< j << std::endl;
			}
			//argos::LOG <<"*****TRIAL_" << i <<" ***** "<<" Footbot "<< j <<" xPos is "<<posX<< " yPos is "<<posY<< std::endl;
			//argos::LOG <<"*****TRIAL_" << i <<" ***** "<<" Footbot "<< j <<" radius is "<<radius<< " angle is "<<angle<< std::endl;
			m_vecInitSetup.push_back(fbSetup);
		}
	}
	
	
}
/****************************************/


bool CEvolutionLoopFunctions::Collision(SInitSetup fbSetupGuess, int footbotIndex, UInt32 trial) {
	UInt32 currentVecSize = m_vecInitSetup.size(); //current size of the vector
	UInt32 base = trial*NUM_FOOTBOT;
	for(int i=base;i<base+footbotIndex;i++){//confront with other footbot in same trial
		//(r1+r2)^2 >= (x2-x1)^2+(y2-y1)^2
		float xDiff =  fbSetupGuess.Position.GetX() - m_vecInitSetup[i].Position.GetX();
		float yDiff =  fbSetupGuess.Position.GetY() - m_vecInitSetup[i].Position.GetY();
		float centerDiff = pow(xDiff,2) + pow(yDiff,2);
		//argos::LOG <<"Confronto per footbot "<< footbotIndex << " con " << i-base <<" POWRAD " << pow(2*FOOTBOT_RADIUS,2) << " centerDiff " << centerDiff << " e i? "<< i <<std::endl;
		if((pow(2*FOOTBOT_RADIUS,2) >= centerDiff)){
			argos::LOG <<"Collisione per footbot "<< footbotIndex << " con " << i-base <<std::endl;
			return true;
		}
	}
	/*
	CVector2 rect_north[4] = {CVector2(0.7f,0.0f),CVector2(0.7f,0.1f),CVector2(-0.7f,0.1f),CVector2(-0.7f,0.0f)};
	CVector2 rect_east[4] = {CVector2(-0.7f,0.0f),CVector2(-0.69f,0.0f),CVector2(-0.7f,-0.35f),CVector2(-0.69f,0.35f)};
	CVector2 rect_west[4] = {CVector2(0.7f,0.0f),CVector2(0.69f,0.0f),CVector2(0.7f,-0.35f),CVector2(0.69f,-0.35f)};
	*/
	if(IsInsideForbiddenCircle(fbSetupGuess.Position.GetX(),fbSetupGuess.Position.GetY())){
		argos::LOG <<"Collisione per footbot "<< footbotIndex << " con forbidden area"<<std::endl;
		return true;
	}
	
	return false;
}

bool CEvolutionLoopFunctions::IsInsideForbiddenCircle(Real pos_x , Real pos_y){
	if(std::abs(pos_x) <= (2*FOOTBOT_RADIUS) && std::abs(pos_y) <= (2*FOOTBOT_RADIUS)){
		argos::LOG <<"Dentro forbidden quindi calcolane un altro "<<std::endl;
		return true;
	}
	return false;
}


/*
* In PostStep() we evaluate the performance in the single step, summing it
* P = SUM foreach t from 0 to T of N(t) where N(t) is the number of robot in the shelter at the time t
*/

void CEvolutionLoopFunctions::PostStep() {
	/*int insideFootbot = 0;
	for(int i=0; i < NUM_FOOTBOT; i++){
		CVector2 cPos; //footbot pos wrt the origin
		cPos.Set(m_pcFootBot[i]->GetEmbodiedEntity().GetOriginAnchor().Position.GetX(), m_pcFootBot[i]->GetEmbodiedEntity().GetOriginAnchor().Position.GetY());
		//if(IsInsideRect(cPos)){
			count_footbot_inside++;
			insideFootbot++;
		//}
	}
	if(isVisual){
		LOG << " Inside footbot num = " << insideFootbot << std::endl;
		LOG << " Total performance = " << count_footbot_inside << std::endl;
	}*/
}


/****************************************/

void CEvolutionLoopFunctions::Reset() {
	performance = 0; //reset footbot count
	/*
	 * Move robot to the initial position corresponding to the current trial
	 */
	for(int i=0; i < NUM_FOOTBOT; i++){
		if(!MoveEntity(
			m_pcFootBot[i]->GetEmbodiedEntity(),             // move the body of the robot
			m_vecInitSetup[(m_unCurrentTrial*NUM_FOOTBOT)+i].Position,    // to this position
			m_vecInitSetup[(m_unCurrentTrial*NUM_FOOTBOT)+i].Orientation, // with this orientation
			false                                         // this is not a check, leave the robot there
			)) 
		{
 				LOGERR << m_unCurrentTrial <<" ///RESET///"<< i <<"foot bot POS PRE<"
						<< m_pcFootBot[i]->GetEmbodiedEntity().GetOriginAnchor().Position.GetX()
						<< ">, <"
						<< m_pcFootBot[i]->GetEmbodiedEntity().GetOriginAnchor().Position.GetY()
						<< ">"
						<< std::endl;
				LOGERR << m_unCurrentTrial<<" ///RESET///"<< i <<"Can't move robot in <"
						<< m_vecInitSetup[(m_unCurrentTrial*NUM_FOOTBOT)+i].Position
						<< ">, <"
						<< m_vecInitSetup[(m_unCurrentTrial*NUM_FOOTBOT)+i].Orientation
						<< ">"
						<< std::endl;
				LOGERR << m_unCurrentTrial <<" ///RESET///"<< i <<"foot bot POS POST<"
						<< m_pcFootBot[i]->GetEmbodiedEntity().GetOriginAnchor().Position.GetX()
						<< ">, <"
						<< m_pcFootBot[i]->GetEmbodiedEntity().GetOriginAnchor().Position.GetY()
						<< ">"
						<< std::endl;
			}
	}
}

/****************************************/
/*Get the floor color to draw the circles and the rectangle*/
/****************************************/

CColor CEvolutionLoopFunctions::GetFloorColor(const CVector2& c_position_on_plane) {
	if(IsInsideCircle(c_position_on_plane)) {
		return CColor::BLACK;
	}
	return CColor::GRAY50;
}

bool CEvolutionLoopFunctions::IsInsideCircle(const CVector2& c_position_on_plane){
	for(UInt32 i = 0; i < m_cCirclePos.size(); ++i) {
		if((c_position_on_plane - m_cCirclePos[i]).Length() < CIRCLE_RADIUS) {
			return true;
		}
	}
	return false;
}


/****************************************/
/****************************************/

void CEvolutionLoopFunctions::ConfigureFromGenome(const GARealGenome& c_genome) {
	//for each footbot we set the perceptron controller
	for(UInt32 j = 0; j < NUM_FOOTBOT ; j++){
		/* Copy the genes into the NN parameter buffer */
		for(size_t i = 0; i < GENOME_SIZE; ++i) {
			m_pfControllerParams[i] = c_genome[i];
		}
		/* Set the NN parameters */
		m_pcController[j]->GetPerceptron().SetOnlineParameters(GENOME_SIZE, m_pfControllerParams);
	}
}

/****************************************/
/****************************************/

Real CEvolutionLoopFunctions::Performance() {
	return performance;
}


void CEvolutionLoopFunctions::PostExperiment (){
	//Real CalculateObjectiveFunction() { 
	// Total distance between each selected random point and its closest robots 

	//Real d = 0; 
	// Expected distance 
	//Real e = 0; 
	//size_t i; 
	//for (i=0; i<10000; ++i) { 
		//CVector2 vRandomPoint = GetRandomPointInArena(); 
		//d += GetClosestEpuckDistanceFromPoint(vRandomPoint);
	//} 
	//e = d/i; 
	//return e; }
	//argos::LOG <<"PostExperiment"<<std::endl;

	Real d = 0; 
	Real e = 0;

	size_t i;
	for(i=0; i < 10000; ++i){
		CRange<Real> range;
		range.SetMin(MIN_RANGE_POS);
		range.SetMax(MAX_RANGE_POS);
		Real x = m_pcRNG->Uniform(range);
		Real y = m_pcRNG->Uniform(range);
		CVector2 random_vector(x,y);
		d += GetClosestFootBotDistanceFromPoint(random_vector);
		//argos::LOG <<"** d **" << d<< std::endl;
	}
	e = d/i; //media
	//argos::LOG <<"## e ##" << e<< std::endl;
	performance = e; 
	//argos::LOG <<"Aggiorno trial "<<m_unCurrentTrial <<std::endl;

}

Real CEvolutionLoopFunctions::GetClosestFootBotDistanceFromPoint (const CVector2& c_position){
	Real distance = 10.0f;
	for(int i=0; i < NUM_FOOTBOT; i++){
		CVector3 position_ref = m_pcFootBot[i]->GetEmbodiedEntity().GetOriginAnchor().Position;
		CVector2 vec(position_ref.GetX(),position_ref.GetY());
		Real dist = (c_position - vec).Length();

		//argos::LOG <<position_ref.GetX()<< " "<<position_ref.GetY()<<std::endl;
		//argos::LOG <<c_position.GetX()<< " "<<c_position.GetY()<<std::endl;
		//argos::LOG <<"dist ---- "<<dist<<std::endl<<"--------------"<<std::endl;
		
		if(dist < distance && !IsInsideCircle(vec)){
			distance = dist;
		}

	}
	return distance;
}

/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(CEvolutionLoopFunctions, "evolution_loop_functions")
