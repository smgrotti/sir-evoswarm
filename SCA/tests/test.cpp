/* GA-related headers */
#include <ga/ga.h>

/* ARGoS-related headers */
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/simulator/loop_functions.h>
//include header constants
#include <embedding/galib/evolution_kb.h>

#include <loop_functions/evolution_loop_functions/evolution_loop_functions.h>

#include <algorithm>
#include <iostream>
#include <fstream>

/*Main class that executes NUM_TRIAL tests for a certain perceptron (specified in ev_test.argos.)
 *Creates the .csv file and prints mean and median.
 */
std::ofstream file_performances;
/****************************************/
/****************************************/
/*Print an array of int*/
template <typename Type>
Type PrintArray(Type array[], size_t lenght){
	for(size_t i = 0; i < lenght; i++){
		argos::LOG << array[i] <<",";
	}
	argos::LOG << std::endl;
}

/* Computes the median of an array of value */
template <typename Type>
Type MedianValue(Type array[], size_t lenght){
	std::sort(array, array+NUM_TRIAL); //where n is the number of elements you want to sort
	if(lenght % 2 == 0){ //if lenght is even
		return (array[(lenght/2)-1] + array[lenght/2]) / 2; //median value
	} else { 			//if lenght is odd
		return array[lenght/2];
	}
}

/* Computes the min of an array of value */
template <typename Type>
Type MinValue(Type array[], size_t lenght){
	std::sort(array, array+NUM_TRIAL); //where n is the number of elements you want to sort
	return array[0];

}

/* Computes the max of an array of value */
template <typename Type>
Type MaxValue(Type array[], size_t lenght){
	std::sort(array, array+NUM_TRIAL); //where n is the number of elements you want to sort
	return array[lenght-1];
}

///* Computes the mean of an array of value */
//float MeanValue(UInt32 array[], size_t lenght){
//	float sum = 0.0f;
//	for(size_t i = 0; i < lenght ; i++){
//		sum += array[i];
//	}
//	return sum / lenght;
//}

/*
 * Launche the simulator for NUM_TRIAL times in order to evaluate a certain perceptron
 * and retrieve performance data.
 */
void LaunchARGoS() {
	static argos::CSimulator& cSimulator = argos::CSimulator::GetInstance();
	
	static CEvolutionLoopFunctions& cLoopFunctions = dynamic_cast<CEvolutionLoopFunctions&>(cSimulator.GetLoopFunctions());
	
	UInt32 performances[NUM_TRIAL];
	for(size_t i = 0; i < NUM_TRIAL; ++i) {
		//execute test i
		cLoopFunctions.SetTrial(i);
		cSimulator.Reset();
		cSimulator.Execute();
		performances[i] = cLoopFunctions.Performance();
		
		argos::LOG << "Performance trial: "<< i << " is "<< performances[i] <<std::endl;
			
		if (file_performances.is_open()) {
			file_performances << cLoopFunctions.Performance() 
			<< ";"
			<<std::flush; 	// with the flush function we force the 
							// synchronization and all the data is written 
							// to the physical medium.
		}
	}
	argos::LOG << "Median performance: "<< MedianValue(performances, NUM_TRIAL) <<std::endl;
	if (file_performances.is_open()) {
			file_performances << std::endl //inserts a newline character
			<<std::flush;
	}
}


/****************************************/
/****************************************/

int main(int argc, char** argv) {
	
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	time (&rawtime);
	timeinfo = localtime (&rawtime);

	strftime (buffer,80,"%m-%d_%H-%M",timeinfo);
	argos::LOG<<buffer<<std::endl;

	std::string str(buffer);
	std::ostringstream filename;
	filename 	<<"TEST_"<< str 	<< "_"
				<< "p"  << POPULATION_SIZE << "_"
				<< "b"	<< (POPULATION_SIZE/ELITISM_FRACTION) << "_"
				<< "t"	<< NUM_TRIAL << "_"
				<< "c"	<< PROB_CROSSOVER << "_"
				<< "m" 	<< PROB_MUTATION
				<< ".csv";

	file_performances.open (filename.str().c_str(), std::ios::out | std::ios::trunc); //It opens a file to store the perfomances, with the date and time as name.
	
	argos::CSimulator& cSimulator = argos::CSimulator::GetInstance();
	cSimulator.SetExperimentFileName("tests/ev-test.argos");
	cSimulator.LoadExperiment();
	
	LaunchARGoS(); //launch the simulator NUM_TRIAL times
	
	file_performances.close();
		
		
	cSimulator.Destroy();
	return 0;
}

/****************************************/
/****************************************/
