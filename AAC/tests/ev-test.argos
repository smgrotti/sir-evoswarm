<?xml version="1.0" ?>

<!-- *************************************************** -->
<!-- * A fully commented XML is diffusion_1.xml. Refer * -->
<!-- * to it to have full information about what       * -->
<!-- * these options mean.                             * -->
<!-- *************************************************** -->

<argos-configuration>

  <!-- ************************* -->
  <!-- * General configuration * -->
  <!-- ************************* -->
  <framework>
    <system threads="0" />
    <!-- Each experimental run is 120 seconds long -->
    <experiment length="120"
                ticks_per_second="10"
                random_seed="313" />
  </framework>

  <!-- *************** -->
  <!-- * Controllers * -->
  <!-- *************** -->
  <controllers>

    <footbot_nn_controller id="fnn"
                           library="build/controllers/footbot_nn/libfootbot_nn">
      <actuators>
        <differential_steering implementation="default" />
      </actuators>
      <sensors>
        <footbot_proximity        implementation="default"    show_rays="false" />
        <footbot_light            implementation="rot_z_only" show_rays="false" />
        <footbot_motor_ground     implementation="rot_z_only" show_rays="false" />
      </sensors>
      <params num_inputs="52"
              num_outputs="2" 
              parameter_file="results/genomes/best_3.dat"/>
    </footbot_nn_controller>

  </controllers>

  <!-- ****************** -->
  <!-- * Loop functions * -->
  <!-- ****************** -->
  <loop_functions library="build/loop_functions/evolution_loop_functions/libevolution_loop_functions"
                  label="evolution_loop_functions" />

  <!-- *********************** -->
  <!-- * Arena configuration * -->
  <!-- *********************** -->
  <arena size="6,6,3" center="0,0,0">

    <!--
        Here we just put the static elements of the environment (the walls
        and the light).
        The dynamic ones, in this case the foot-bot, are placed by the
        loop functions at the beginning of each experimental run.
    -->

	<!--SGMOD Added floor entity-->
    <floor id="floor"
           source="loop_functions"
           pixels_per_meter="100" />
	<!--SGMOD new dodecagonal arena-->
	<box id="wall_0" size="1.52,0.1,0.5" movable="false">
		<body position="0,2.84,0" orientation="0,0,0" />
	</box>
	<box id="wall_1" size="1.52,0.1,0.5" movable="false">
		<body position="1.42,2.46,0" orientation="-30,0,0" />
	</box>
	<box id="wall_2" size="1.52,0.1,0.5" movable="false">
		<body position="2.46,1.42,0" orientation="-60,0,0" />
	</box>
	<box id="wall_3" size="1.52,0.1,0.5" movable="false">
		<body position="2.84,0,0" orientation="-90,0,0" />
	</box>
	<box id="wall_4" size="1.52,0.1,0.5" movable="false">
		<body position="2.46,-1.42,0" orientation="-120,0,0" />
	</box>
	<box id="wall_5" size="1.52,0.1,0.5" movable="false">
		<body position="1.42,-2.46,0" orientation="-150,0,0" />
	</box>
	<box id="wall_6" size="1.52,0.1,0.5" movable="false">
		<body position="0,-2.84,0" orientation="-180,0,0" />
	</box>
	<box id="wall_7" size="1.52,0.1,0.5" movable="false">
		<body position="-1.42,-2.46,0" orientation="-210,0,0" />
	</box>
	<box id="wall_8" size="1.52,0.1,0.5" movable="false">
		<body position="-2.46,-1.42,0" orientation="-240,0,0" />
	</box>
	<box id="wall_9" size="1.52,0.1,0.5" movable="false">
		<body position="-2.84,0,0" orientation="-270,0,0" />
	</box>
	<box id="wall_10" size="1.52,0.1,0.5" movable="false">
		<body position="-2.46,1.42,0" orientation="-300,0,0" />
	</box>
	<box id="wall_11" size="1.52,0.1,0.5" movable="false">
		<body position="-1.42,2.46,0" orientation="-330,0,0" />
	</box>
	
	<light id="light"
		position="0,-2.84,0.8"
		orientation="0,0,0"
		color="yellow"
		intensity="3"
		medium="leds" />
		
  </arena>

  <!-- ******************* -->
  <!-- * Physics engines * -->
  <!-- ******************* -->
  <physics_engines>
    <dynamics2d id="dyn2d" />
  </physics_engines>

  <!-- ********* -->
  <!-- * Media * -->
  <!-- ********* -->
  <media>
    <led id="leds" />
  </media>

  <!-- ****************** -->
  <!-- * Visualization * -->
  <!-- ****************** -->
  <!-- We don't want nor need a visualization during evolution -->
    <visualization/>

</argos-configuration>
