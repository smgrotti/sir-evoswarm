/* GA-related headers */
#include <ga/ga.h>

/* ARGoS-related headers */
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/simulator/loop_functions.h>
//include header constants
#include <embedding/galib/evolution_kb.h>

#include <loop_functions/evolution_loop_functions/evolution_loop_functions.h>

#include <algorithm>

#include <iostream>
#include <fstream>


std::ofstream file_performances; //.csv of the results obtained by each genome
//std::ofstream file_populations; //file for population

size_t iterator = 0;
size_t generation = 0;
bool first_time = 1;


std::vector<UInt32> current_population;

UInt32 best_median = 0;
GARealGenome* best_genome; 


bool MySort (UInt32 i,UInt32 j) { return (i>j); }

/****************************************/
/****************************************/
/*Print an array of int*/
template <typename Type>
Type PrintArray(Type array[], size_t lenght){
	for(size_t i = 0; i < lenght; i++){
		argos::LOG << array[i] <<",";
	}
	argos::LOG << std::endl;
}

/* Computes the median of an array of value */
template <typename Type>
Type MedianValue(Type array[], size_t lenght){
	std::sort(array, array+NUM_TRIAL); //where n is the number of elements you want to sort
	if(lenght % 2 == 0){ //if lenght is even
		return (array[(lenght/2)-1] + array[lenght/2]) / 2; //median value
	} else { 			//if lenght is odd
		return array[lenght/2];
	}
}

/* Computes the min of an array of value */
template <typename Type>
Type MinValue(Type array[], size_t lenght){
	std::sort(array, array+NUM_TRIAL); //where n is the number of elements you want to sort
	return array[0];

}

/* Computes the max of an array of value */
template <typename Type>
Type MaxValue(Type array[], size_t lenght){
	std::sort(array, array+NUM_TRIAL); //where n is the number of elements you want to sort
	return array[lenght-1];
}

/*
 * Flush to file a genome of a generation
 */
void SaveGenome(const GARealGenome& c_genome, int un_generation, int n) {
	std::ostringstream cOSS;
	cOSS << RES_DIR<<GENOMES_DIR<<"generation_" << un_generation <<"genome_"<< n << ".dat";
	std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::trunc);
	cOFS << GENOME_SIZE
	<< " "
	<< c_genome
	<< std::endl;
}


/*
 * Flush best individual
 */
void FlushBest(const GARealGenome& c_genome, size_t un_generation) {
	std::ostringstream cOSS;
	cOSS << RES_DIR<<GENOMES_DIR<<"best_" << un_generation << ".dat";
	std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::trunc);
	cOFS << GENOME_SIZE // first write the number of values to dump
	<< " "
	<< c_genome    // then write the actual values
	<< std::endl;
}

/*
 * Launch ARGoS to evaluate a genome.
 */
float LaunchARGoS(GAGenome& c_genome) { //[MB]-> funzione di valutazione
	argos::LOG << "Starting evaluation for genome "<< iterator <<" OF generation "<< generation <<std::endl;
	/* Convert the received genome to the actual genome type */
	GARealGenome& cRealGenome = dynamic_cast<GARealGenome&>(c_genome);
	/* The CSimulator class of ARGoS is a singleton. Therefore, to
	* manipulate an ARGoS experiment, it is enough to get its instance.
	* This variable is declared 'static' so it is created
	* once and then reused at each call of this function.
	* This line would work also without 'static', but written this way
	* it is faster. */
	static argos::CSimulator& cSimulator = argos::CSimulator::GetInstance();
	/* Get a reference to the loop functions */
	static CEvolutionLoopFunctions& cLoopFunctions = dynamic_cast<CEvolutionLoopFunctions&>(cSimulator.GetLoopFunctions());
	
	//PRINT THE GENOME TO THE FILE POPULATIONS
//	if (file_populations.is_open()) {
//			file_populations << cRealGenome << std::endl <<std::flush;
//	}
	
	//PRINT THE GENOME TO THE DIR
//	SaveGenome(cRealGenome,generation,iterator);
	
	UInt32 performances[NUM_TRIAL];
	for(size_t i = 0; i < NUM_TRIAL; ++i) {
		/* Tell the loop functions to get ready for the i-th trial */
		cLoopFunctions.SetTrial(i);
		/* Reset the experiment.
		 * This internally calls also CEvolutionLoopFunctions::Reset(). */
		cSimulator.Reset();
		/* Configure the controller with the genome */
		cLoopFunctions.ConfigureFromGenome(cRealGenome);
		/* Run the experiment */
		cSimulator.Execute();
		/* Update performance */
		//argos::LOG << "Performance Trial "<< i <<": "<< cLoopFunctions.Performance() <<std::endl;
		performances[i] = cLoopFunctions.Performance();
	}	
	
	iterator++;

	UInt32 t_median = MedianValue(performances, NUM_TRIAL);
	if (t_median >= best_median)
	{	
		//argos::LOG <<"valore mediano migliore "<<t_median<< " di: "<<best_median<<std::endl;
		best_median = t_median;
		best_genome = &cRealGenome;
	}
		
	current_population.push_back(t_median);

	if(iterator == POPULATION_SIZE && first_time){
		first_time = 0; //set to FALSE
		// sorting
		sort(current_population.begin(), current_population.end(), MySort);
		// we print to file
		for(int i = 0; i<POPULATION_SIZE; i++){
			if (file_performances.is_open()) {
				file_performances << current_population[i];
				if (i != POPULATION_SIZE - 1)
				{
					file_performances << ";";
				}
				file_performances <<std::flush;
			}
		}
		if (file_performances.is_open()) {
			file_performances << std::endl //inserts a newline character
			<<std::flush;
		}
		current_population.resize(POPULATION_SIZE/ELITISM_FRACTION);
		//save the best
		FlushBest(*best_genome,generation);
		//update the generation
		generation++;
	} else if (iterator == (POPULATION_SIZE-(POPULATION_SIZE/ELITISM_FRACTION)) && !first_time){
		// sorting 
		sort(current_population.begin(), current_population.end(), MySort);
		// we print to file
		for(int i = 0; i<POPULATION_SIZE; i++){
			if (file_performances.is_open()) {
				file_performances << current_population[i];
				if (i != POPULATION_SIZE - 1)
				{
					file_performances << ";";
				}
				file_performances <<std::flush;
			}
		}
		if (file_performances.is_open()) {
			file_performances << std::endl //inserts a newline character
			<<std::flush;
		}
		current_population.resize(POPULATION_SIZE/ELITISM_FRACTION);
		//save the best
		FlushBest(*best_genome,generation);
		
		// update the generation
		generation++;
	}
	/* Return the median value of the evaluation */
	return t_median+0.0f;
}

/****************************************/
/****************************************/

int main(int argc, char** argv) {
	
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	time (&rawtime);
	timeinfo = localtime (&rawtime);

	strftime (buffer,80,"%m-%d_%H-%M",timeinfo);
	argos::LOG<<buffer<<std::endl;

	std::string str(buffer);
	std::ostringstream filename;
	filename 	<< str 	<< "_"
				<< "p"  << POPULATION_SIZE << "_"
				<< "b"	<< (POPULATION_SIZE/ELITISM_FRACTION) << "_"
				<< "t"	<< NUM_TRIAL << "_"
				<< "c"	<< PROB_CROSSOVER << "_"
				<< "m" 	<< PROB_MUTATION
				<< ".csv";
	
//	std::ostringstream populations_filename;
//	populations_filename <<"population_" << str << ".txt";

	file_performances.open (filename.str().c_str(), std::ios::out | std::ios::trunc); //It opens a file to store the perfomances, with the date and time as name.
//	file_populations.open(populations_filename.str().c_str(), std::ios::out | std::ios::trunc);

	/*
	 * Initialize GALIB
	 */
	/* Create an allele whose values can be in the range [-10,10] */
	GAAlleleSet<float> cAlleleSet(-10.0f, 10.0f);
	/* Create a genome, using LaunchARGoS() to evaluate it */
	GARealGenome cGenome(GENOME_SIZE, cAlleleSet, LaunchARGoS);
	/* Create and configure a basic genetic algorithm using the genome */
	GASteadyStateGA cGA(cGenome);
	cGA.maximize();                     									// the objective function must be maximized
	cGA.nReplacement(POPULATION_SIZE-(POPULATION_SIZE/ELITISM_FRACTION)); 	// 1/ELITISM_FRACTION of the population
	cGA.populationSize(POPULATION_SIZE);              						// population size for each generation
	cGA.nGenerations(N_GENERATIONS);              							// number of generations
	cGA.pMutation(PROB_MUTATION);               							// prob of gene mutation
	cGA.pCrossover(PROB_CROSSOVER);
	cGA.nBestGenomes(N_BEST_GENOMES);
	//cGA.scoreFilename("evolution.dat"); 									// filename for the result log
	cGA.flushFrequency(0);             							 			// log the results every generation
	//set to 0 because we handle manually data retrieval
	
	argos::LOG<<"NUM OF BEST IS: "<<cGA.nBestGenomes()<<std::endl;
	argos::LOG<<"pCrossover is: "<<cGA.pCrossover()<<std::endl;
	argos::LOG<<"pMutation is: "<<cGA.pMutation()<<std::endl;
	argos::LOG<<"num of replacement is: "<<cGA.nReplacement()<<std::endl;
	argos::LOG<<"NUM of pop is: "<<cGA.populationSize()<<std::endl;
	argos::LOG<<"cga freq is : "<<cGA.flushFrequency()<<std::endl;
	
	/*
	 * Initialize ARGoS
	 */
	/* The CSimulator class of ARGoS is a singleton. Therefore, to
	 * manipulate an ARGoS experiment, it is enough to get its instance */
	argos::CSimulator& cSimulator = argos::CSimulator::GetInstance();
	/* Set the .argos configuration file
	* This is a relative path which assumed that you launch the executable
	* from argos3-examples (as said also in the README) */
	cSimulator.SetExperimentFileName("experiments/evolution.argos");
	/* Load it to configure ARGoS */
	cSimulator.LoadExperiment();
	/*
	* Launch the evolution, setting the random seed
	*/
	/*
	*   [MB]->The evolve member function first calls initialize then calls the step member function 
	*   until the done member function returns gaTrue. It calls the flushScores member as needed 
	*   when the evolution is complete. If you evolve the genetic algorithm without using the 
	*   evolve member function, be sure to call initialize before stepping through the evolution. 
	*   You can use the step member function to evolve a single generation. 
	*   You should call flushScores when the evolution is finished so that any buffered scores are flushed.   
	*/
	
	argos::LOG << "Initialize the Genetic." <<std::endl;
	
	cGA.initialize(12345);      /* [MB]-> INITIALIZE: Initialize the genetic algorithm. If you specify a seed, 
		                        * this function calls GARandomSeed with that value. If you do not specify a 
		                        * seed, GAlib will choose one for you as described in the random functions 
		                        * section. It then initializes the population and does the first population 
		                        * evaluation.
		                        */
	
	argos::LOG << "Generation #0 DONE." <<std::endl;
	
	do {
		iterator = 0;
		
		cGA.step();
		argos::LOG << "Generation #" << cGA.generation() << " DONE" << std::endl;
		
//		if(cGA.generation() % cGA.flushFrequency() == 0) {
//			argos::LOG << "   Flushing...";
//			/* Flush scores */
//			//cGA.flushScores();
//			argos::LOG << "done.";
//		}
		LOG << std::endl;
		LOG.Flush();
		
	}
	while(! cGA.done());


	file_performances.close();
//	file_populations.close();

	/*
	* Dispose of ARGoS stuff
	*/
	cSimulator.Destroy();

	/* All is OK */
	return 0;
}

/****************************************/
/****************************************/
