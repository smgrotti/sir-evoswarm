/* The NN controller */
#include <controllers/footbot_nn/footbot_nn_controller.h>

/* ARGoS-related headers */
#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/utility/math/rng.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>

//include header constants
#include <embedding/galib/evolution_kb.h>

/* GA-related headers */
#include <ga/ga.h>
#include <ga/GARealGenome.h>
#include <ga/GARealGenome.C> // this is necessary!

#include <math.h>

/****************************************/
/****************************************/

/*
 * The size of the genome.
 * 
 * The genome is the set of NN weights. The NN is a simple
 * 2-layer perceptron. The inputs are 24 proximity readings and
 * 24 light readings and 4 motor ground sensor. The outputs are 2 wheels speeds. The total
 * number of weights is therefore:
 *
 * W = (I + 1) * O = (24 + 24 + 4 + 1) * 2 = 106
 *
 * where:
 *   W = number of weights
 *   I = number of inputs
 *   O = number of outputs
 */
static const size_t GENOME_SIZE = 106;

/****************************************/
/****************************************/

using namespace argos;

class CEvolutionLoopFunctions : public CLoopFunctions {

public:

	CEvolutionLoopFunctions();
	virtual ~CEvolutionLoopFunctions();

	virtual void Init(TConfigurationNode& t_node);
	virtual void PostStep();
	virtual void Reset();
	/*SGMOD added function to cet the floor color*/
	virtual CColor GetFloorColor(const CVector2& c_position_on_plane);

	/* Called by the evolutionary algorithm to set the current trial */
	inline void SetTrial(size_t un_trial) {
	  m_unCurrentTrial = un_trial;
	}

	/* Configures the robot controller from the genome */
	void ConfigureFromGenome(const GARealGenome& c_genome);

	/* Calculates the performance (counting inside footbot) of the robot in a trial */
	UInt32 Performance();
	
private:

	/* The initial setup of a trial */
	struct SInitSetup {
	  CVector3 Position;
	  CQuaternion Orientation;
	};

	size_t m_unCurrentTrial;
	std::vector<SInitSetup> m_vecInitSetup;
	std::vector<CFootBotEntity*> m_pcFootBot;
	std::vector<CFootBotNNController*> m_pcController;
	Real* m_pfControllerParams;
	CRandom::CRNG* m_pcRNG;
	
	std::vector<CVector2> m_cCirclePos;
	CFloorEntity* m_pcFloor;
	UInt32 count_footbot_inside; //footbot count


	/*utility functions*/
	bool IsInsideForbiddenCircle(Real pos_x , Real pos_y);
	bool IsInsideCircle(const CVector2& c_position_on_plane);
	bool IsInsideRect(const CVector2& c_position_on_plane);
	void GeneratePositions(UInt32 max_trial);
	bool Collision(SInitSetup fbSetupGuess, int footbotIndex, UInt32 trial);
	bool IntersectRect(CVector3 pos, CVector2 verts[4]);
	bool IntersectRect(CVector3 pos, float sideNorth,float sideEast,float sideSouth,float sideWest);
};
